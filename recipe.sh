#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" 1>&2
    exit 1
}

[ $# -eq 2 ] || die "usage: revision out_dir"
revision=$1; shift
out_dir=$1; shift

set -euo pipefail

exec_msys_arm64()
{
    # execute as login (-l) allows to setup CLANGARM64 env
    # force through cmd.exe to nest out of current msys2 env
    cmd.exe /c "set MSYSTEM=CLANGARM64 && bash.exe -lc \"env PATH=/clangarm64/bin:\$PATH $*\""
    cmd.exe /c "taskkill /IM gpg-agent.exe /f" || true
}

setup_msys_arm64()
{
    local url="https://github.com/msys2/msys2-installer/releases/download/2022-12-16/msys2-base-x86_64-20221216.sfx.exe"
    wget -q $url -O msys2.exe
    ./msys2.exe
    # avoid clear screen on first connect
    sed -e '/clear/d' -i msys64/etc/profile
    cat >> msys64/etc/pacman.conf << EOF
SigLevel = Never
[clangarm64]
Include = /etc/pacman.d/mirrorlist.mingw
EOF
    exec_msys_arm64 pacman-key --init
    exec_msys_arm64 pacman-key --populate
    exec_msys_arm64 pacman -Sy
    exec_msys_arm64 pacman -S base-devel --noconfirm
}

checkout()
{
    local revision=$1
    if [ -d mpv ]; then
        return
    fi
    git clone https://github.com/mpv-player/mpv
    pushd mpv
    git checkout $revision
    git log -n1
    popd
}

get_dependencies()
{
    done=deps/.done
    export PATH=$(pwd)/deps/msys64/usr/bin:$PATH

    if [ -f $done ]; then
        return
    fi

    rm -rf deps
    mkdir -p deps
    pushd deps

    setup_msys_arm64
    export MINGW_PACKAGE_PREFIX=mingw-w64-clang-aarch64
makedepends=(
  "${MINGW_PACKAGE_PREFIX}-meson"
  "${MINGW_PACKAGE_PREFIX}-python"
  "${MINGW_PACKAGE_PREFIX}-pkgconf"
  "${MINGW_PACKAGE_PREFIX}-clang"
  "${MINGW_PACKAGE_PREFIX}-cmake")
depends=(
  "${MINGW_PACKAGE_PREFIX}-ffmpeg"
  "${MINGW_PACKAGE_PREFIX}-libjpeg-turbo"
  "${MINGW_PACKAGE_PREFIX}-spirv-cross"
  "${MINGW_PACKAGE_PREFIX}-shaderc")

    exec_msys_arm64 pacman -S --noconfirm "${makedepends[@]}" "${depends[@]}"
    exec_msys_arm64 pacman -S --noconfirm \
        git ninja python ${MINGW_PACKAGE_PREFIX}-cc
    popd
    touch $done
}

build()
{
    script_dir=$(dirname $(readlink -f $0))
    pushd mpv
    if [ ! -d build ]; then
        cp $script_dir/clang-arm64.txt .
        exec_msys_arm64 meson setup build --prefix=/clangarm64 --cross-file=clang-arm64.txt -Dgl=disabled
    fi
    exec_msys_arm64 meson compile -C build
    popd
}

checkout $revision
get_dependencies
build
