import argparse
import shutil
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import cygwin_path, run

DEFAULT_REVISION = "master"
RECIPE_DIR = dirname(__file__)


class Recipe(PackageShortRecipe):
    def __init__(self, revision):
        self.revision = revision

    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="Build MPV",
            id="mpv",
            pretty_name="MPV",
            version=self.revision,
        )

    def all_steps(self, out_dir: str):
        bash = shutil.which("bash")
        out_dir = cygwin_path(out_dir)
        recipe = cygwin_path(join(RECIPE_DIR, "recipe.sh"))
        run(bash, recipe, self.revision, out_dir)


parser = argparse.ArgumentParser(description="Build MPV")
parser.add_argument("--revision", help="Revision to build", default=DEFAULT_REVISION)
args = parser.parse_args()
PackageBuilder(Recipe(args.revision)).make()
